{
  description = "A basic Haskell flake for Advent of Code";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";
  };

  outputs = { self, nixpkgs }: {
    devShell.x86_64-linux =
      let
        pkgs = nixpkgs.legacyPackages.x86_64-linux;

        ghc = pkgs.haskell.packages.ghc924.ghcWithHoogle (self: with self;
          [
            relude
            split

            aeson
            random
            neat-interpolation
          ]
        );
      in
      pkgs.mkShell {
        buildInputs = with pkgs.haskellPackages; [
          ghc

          haskell-language-server
          ghcid
          hlint
        ] ++ (with pkgs; [
          nano
        ]);
        shellHook = ''
          echo hi
        '';
      };
  };
}
