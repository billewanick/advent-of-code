{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
{-# LANGUAGE TupleSections #-}

import           Data.List                      ( elemIndex
                                                , transpose
                                                )
import           Data.List.Split                ( splitOn )

-- Part 1

header :: [Int]
header =
  [ 7
  , 4
  , 9
  , 5
  , 11
  , 17
  , 23
  , 2
  , 0
  , 14
  , 21
  , 24
  , 10
  , 16
  , 13
  , 6
  , 15
  , 25
  , 12
  , 22
  , 18
  , 20
  , 8
  , 19
  , 3
  , 26
  , 1
  ]

boards :: [Board]
boards =
  [ [ [22, 13, 17, 11, 0]
    , [8, 2, 23, 4, 24]
    , [21, 9, 14, 16, 7]
    , [6, 10, 3, 18, 5]
    , [1, 12, 20, 15, 19]
    ]
  , [ [3, 15, 0, 2, 22]
    , [9, 18, 13, 17, 5]
    , [19, 8, 7, 25, 23]
    , [20, 11, 10, 24, 4]
    , [14, 21, 16, 12, 6]
    ]
  , [ [14, 21, 17, 24, 4]
    , [10, 16, 15, 9, 19]
    , [18, 8, 23, 26, 20]
    , [22, 11, 13, 6, 5]
    , [2, 0, 12, 3, 7]
    ]
  ]

type Board = [[Int]]

extractBoards :: [String] -> [Board]
extractBoards [] = []
extractBoards (w : a : b : c : d : e : xs) =
  map (map read . words) [a, b, c, d, e] : extractBoards xs

format :: String -> ([Int], [Board])
format xs = (header, boards)
 where
  inputLines = lines xs
  header     = map read $ splitOn "," $ head inputLines
  boards     = extractBoards (tail inputLines)

main :: IO ()
main = do
  entries <- readFile "2021/input4"
  let (header, boards) = format entries

  putStr "Advent of Code Day 4, Part 1: "
  let n = solveP1 header boards
  print n

  putStr "Advent of Code Day 4, Part 2: "
  let n = solveP2 header boards
  print n

mark n (a, b) = if n == a then (a, True) else (a, b)

markBoard n = map (map (mark n))

markAll n = map (markBoard n)

checkNumber :: (Int, Bool) -> Bool
checkNumber (n, b) = b

checkRow row = length (filter checkNumber row) == 5

checkBoard :: [[(Int, Bool)]] -> Bool
checkBoard board = rows || columns
 where
  rows    = any ((== True) . checkRow) board
  columns = any ((== True) . checkRow) $ transpose board

checkAll = map checkBoard

applyNumsTillWin (n : ns) boards =
  let boards'      = markAll n boards
      winningBoard = elemIndex True (checkAll boards')
  in  case winningBoard of
        Nothing -> applyNumsTillWin ns boards'
        Just i  -> (boards' !! i, n)

solveP1 header boards =
  let initBoards = map (map (map (, False))) boards
      (board, n) = applyNumsTillWin header initBoards
      sum        = sumUnmarked board
  in  (n * sum)
 where
  sumUnmarked board = sum $ map fst $ filter (\(n, b) -> not b) (concat board)


-- Part 2
applyNumsTillLastWin (n : ns) [board] =
  let board' = markBoard n board
  in  if checkBoard board'
        then (board', n)
        else applyNumsTillLastWin ns [board']
applyNumsTillLastWin (n : ns) boards =
  let boards'          = markAll n boards
      nonWinningBoards = filter (not . checkBoard) boards'
  in  applyNumsTillLastWin ns nonWinningBoards

solveP2 header boards =
  let initBoards = map (map (map (, False))) boards
      (board, n) = applyNumsTillLastWin header initBoards
      sum        = sumUnmarked board
  in  (n * sum)
 where
  sumUnmarked board = sum $ map fst $ filter (\(n, b) -> not b) (concat board)
