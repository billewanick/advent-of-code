import           Data.List                      ( transpose )

input =
  [ "00100"
  , "11110"
  , "10110"
  , "10111"
  , "10101"
  , "01111"
  , "00111"
  , "11100"
  , "10000"
  , "11001"
  , "00010"
  , "01010"
  ]

main :: IO ()
main = do
  entries <- lines <$> readFile "2021/input3"
  putStr "Advent of Code Day 3, Part 1: "
  print $ solveP1 entries
  -- print $ 4191876 == solveP1 entries -- unit test
  putStr "Advent of Code Day 3, Part 2: "
  print $ solveP2 entries

solveP1 :: [String] -> Int
solveP1 input = gammaRate * epsilonRate
 where
  gammaRate   = toDecimal $ mostCommonBits input
  epsilonRate = toDecimal $ leastCommonBits input

mostCommonBits :: [String] -> [String]
mostCommonBits = map (s . map f) . transpose
 where
  f char = if char == '0' then -1 else 1
  s nums = if sum nums >= 0 then "1" else "0"

leastCommonBits :: [String] -> [String]
leastCommonBits = map (s . map f) . transpose
 where
  f char = if char == '0' then -1 else 1
  s nums = if sum nums < 0 then "1" else "0"

toDecimal :: [String] -> Int
toDecimal str = go 1 0 (reverse str)
 where
  go _    total []       = total
  go base total (x : xs) = go (base * 2) (total + base * read x) xs

--
-- Part 2
--
solveP2 :: [String] -> Int
solveP2 input = oxygenGeneratorRating * cO2ScrubberRating
 where
  oxygenGeneratorRating = toDecimal $ split $ bitMuncher mostCommonBits input
  cO2ScrubberRating     = toDecimal $ split $ bitMuncher leastCommonBits input

bitMuncher :: ([String] -> [String]) -> [String] -> String
bitMuncher bitFunc = go 0
 where
  go _ [x] = x
  go i lst =
    let commonBits = concat $ bitFunc lst
        lst'       = filter (\str -> str !! i == commonBits !! i) lst
    in  go (i + 1) lst'

split :: String -> [String]
-- original
--
-- split []       = []
-- split (x : xs) = [x] : split xs
--
-- after multiple eta reduce
-- hlint is wild
split = map (: [])
