import           Data.List.Split

main :: IO ()
main = do
  entries <- readFile "2021/input7"
  let input = map readInt $ splitOn "," $ head $ lines entries

  putStr "Advent of Code Day 7, Part 1: "
  let n = solveP1 1000 input
  print n

  putStr "Advent of Code Day 7, Part 2: "
  let n = solveP2 1000 input
  print n

readInt :: String -> Int
readInt = read

test :: [Int]
test = [16, 1, 2, 0, 4, 2, 7, 1, 2, 14]

moveToP1 :: Int -> [Int] -> [Int]
moveToP1 n = map (\x -> abs (n - x))

solveP1 :: Int -> [Int] -> Int
solveP1 n input = min
 where
  f         = \n -> sum $ moveToP1 n input
  testRange = [0 .. n]
  sums      = map f testRange
  min       = minimum sums

moveToP2 n = map (\x -> sum [1 .. abs (n - x)])

solveP2 :: Int -> [Int] -> Int
solveP2 n input = min
 where
  f         = \n -> sum $ moveToP2 n input
  testRange = [0 .. n]
  sums      = map f testRange
  min       = minimum sums
