import           Data.List
import           Data.List.Split
import           Data.Maybe

-- 9 is the highest, 0 is the lowest

input :: [[Int]]
input =
  [ [2, 1, 9, 9, 9, 4, 3, 2, 1, 0]
  , [3, 9, 8, 7, 8, 9, 4, 9, 2, 1]
  , [9, 8, 5, 6, 7, 8, 9, 8, 9, 2]
  , [8, 7, 6, 7, 8, 9, 6, 7, 8, 9]
  , [9, 8, 9, 9, 9, 6, 5, 6, 7, 8]
  ]

getPoint :: [[Int]] -> (Int, Int) -> Int
getPoint input (x, y) = (input !! y) !! x

allIndices :: [[Int]] -> [(Int, Int)]
allIndices input =
  [ (x, y)
  | -- 
    x <- [0 .. length (head input) - 1]
  , y <- [0 .. length input - 1]
  ]

getAdjacentPoints :: [[Int]] -> (Int, Int) -> (Int, (Int, Int), [Int])
getAdjacentPoints input (x, y) = (n, (x, y), catMaybes [u, d, l, r])
 where
  n    = gpi (x, y)
  u    = if y <= minY then Nothing else Just (gpi (x, y - 1))
  d    = if y >= maxY then Nothing else Just (gpi (x, y + 1))
  l    = if x <= minX then Nothing else Just (gpi (x - 1, y))
  r    = if x >= maxX then Nothing else Just (gpi (x + 1, y))
  gpi  = getPoint input
  minX = 0
  minY = 0
  maxX = length (head input) - 1
  maxY = length input - 1

allAdjacentPoints :: [[Int]] -> [(Int, (Int, Int), [Int])]
allAdjacentPoints input = map (getAdjacentPoints input) (allIndices input)

isLowPoint :: (Int, (Int, Int), [Int]) -> Bool
isLowPoint (n, _, xs) = all (n <) xs

allLowPoints :: [[Int]] -> [(Int, (Int, Int), [Int])]
allLowPoints = filter isLowPoint . allAdjacentPoints

riskLevel :: Int -> Int
riskLevel = succ

answer :: [[Int]] -> Int
answer = sum . map (riskLevel . fst3) . allLowPoints where fst3 (a, _, _) = a

basinFromLowPoint input (9, (x, y)) = []
basinFromLowPoint input (n, (x, y)) =
  let minX  = 0
      minY  = 0
      maxX  = length (head input) - 1
      maxY  = length input - 1
      gpi   = getPoint input
      u     = if y <= minY then Nothing else Just (x, y - 1)
      d     = if y >= maxY then Nothing else Just (x, y + 1)
      l     = if x <= minX then Nothing else Just (x - 1, y)
      r     = if x >= maxX then Nothing else Just (x + 1, y)
      next  = catMaybes [u, d, l, r]
      next' = map (\d -> (gpi d, d)) next
  in  next'

fs input (n, (x, y)) = go (n, (x, y))
  where go (n, (x, y)) = basinFromLowPoint input

dropLast :: (a, b, c) -> (a, b)
dropLast (a, b, c) = (a, b)

main :: IO ()
main = do
  entries <- readFile "2021/input9"
  let input = map (map readInt . chunksOf 1) $ lines entries

  putStr "Advent of Code Day 9, Part 1: "
  let n = answer input
  print n

  -- let inputP2 = map parseP2Input $ lines entries
  -- putStr "Advent of Code Day 9, Part 2: "
  -- let n' = solveP2 inputP2
  -- print n'

readInt :: String -> Int
readInt = read
