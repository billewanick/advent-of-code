import           Data.List
import           Data.List.Split
import           Data.Maybe

{-
  0:      1:      2:      3:      4:
 aaaa    ....    aaaa    aaaa    ....
b    c  .    c  .    c  .    c  b    c
b    c  .    c  .    c  .    c  b    c
 ....    ....    dddd    dddd    dddd
e    f  .    f  e    .  .    f  .    f
e    f  .    f  e    .  .    f  .    f
 gggg    ....    gggg    gggg    ....

  5:      6:      7:      8:      9:
 aaaa    aaaa    aaaa    aaaa    aaaa
b    .  b    .  .    c  b    c  b    c
b    .  b    .  .    c  b    c  b    c
 dddd    dddd    ....    dddd    dddd
.    f  e    f  .    f  e    f  .    f
.    f  e    f  .    f  e    f  .    f
 gggg    gggg    ....    gggg    gggg


Find the easy digits in seven-segment display

1 uses 2 segments
4 uses 4 segments
7 uses 3 segments
8 uses 8 segments

2 uses 5 segments
3 uses 5 segments
5 uses 5 segments

0 uses 6 segments
6 uses 6 segments
9 uses 6 segments
-}


input :: [String]
input =
  [ "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe"
  , "edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc"
  , "fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg"
  , "fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb"
  , "aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea"
  , "fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb"
  , "dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe"
  , "bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef"
  , "egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb"
  , "gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce"
  ]

parseAP1Input :: String -> [String]
parseAP1Input = splitOn " " . last . splitOn " | "

parseAllP1Input :: [String] -> [[String]]
parseAllP1Input = map parseAP1Input

isEasyDigit :: Int -> Bool
isEasyDigit 2 = True -- is a 1
isEasyDigit 4 = True -- is a 4
isEasyDigit 3 = True -- is a 7
isEasyDigit 7 = True -- is a 8
isEasyDigit _ = False

solveP1 :: [[String]] -> Int
solveP1 = length . concatMap (filter isEasyDigit . map length)

parseP2Input :: String -> ([String], [String])
parseP2Input str =
  let [ss, ns] = splitOn " | " str
      segments = splitOn " " ss
      nums     = splitOn " " ns
  in  (segments, nums)


hasSegments :: Int -> [String] -> String
hasSegments num segs = segs !! index
  where index = fromJust $ findIndex ((num ==) . length) segs


one = hasSegments 2
four = hasSegments 4
seven = hasSegments 3
eight = hasSegments 7

hasNSegs :: Int -> [String] -> [String]
hasNSegs n = filter ((n ==) . length)

has5Segs = hasNSegs 5
has6Segs = hasNSegs 6

-- six is whichever 6 segment that leaves something 
-- behind when subtracting the one segment
six :: [String] -> String
six segs =
  head . filter (\str -> length (one segs \\ str) == 1) . has6Segs $ segs

-- zero is whichever non-6 6 segment that leaves something 
-- behind when subtracting the four segment
zero :: [String] -> String
zero segs =
  let nonSixSegs = filter (/= six segs) $ has6Segs segs
  in  head . filter (\str -> length (four segs \\ str) == 1) $ nonSixSegs

-- nine is whichever 6 segment that isn't six or zero
nine :: [String] -> String
nine segs =
  head $ filter (\str -> str /= six segs && str /= zero segs) $ has6Segs segs

-- three is whichever 5 segment that leaves nothing 
-- behind when subtracting the one segment
three :: [String] -> String
three segs = head . filter (\str -> null (one segs \\ str)) . has5Segs $ segs

-- two is when you subtract 9 from 2 and have 1 left
two :: [String] -> String
two segs =
  let nonThreeSegs = filter (/= three segs) $ has5Segs segs
  in  head . filter (\str -> length (str \\ nine segs) == 1) $ nonThreeSegs

-- five is when you subtract 9 from 5 and have nothing left
five :: [String] -> String
five segs =
  let nonThreeSegs = filter (/= three segs) $ has5Segs segs
  in  head . filter (\str -> null (str \\ nine segs)) $ nonThreeSegs

allNumbers :: [String] -> [String]
allNumbers segs =
  [ zero segs
  , one segs
  , two segs
  , three segs
  , four segs
  , five segs
  , six segs
  , seven segs
  , eight segs
  , nine segs
  ]

decode :: ([String], [String]) -> Int
decode (segs, vals) = read stringList
 where
  sortedNums = map sort $ allNumbers segs
  sortedVals = map sort vals
  mappedInts = map (\val -> fromJust $ elemIndex val sortedNums) sortedVals
  stringList = concatMap show mappedInts

solveP2 :: [([String], [String])] -> Int
solveP2 ls = sum $ map decode ls


main :: IO ()
main = do
  entries <- readFile "2021/input8"
  let inputP1 = parseAllP1Input $ lines entries

  putStr "Advent of Code Day 8, Part 1: "
  let n = solveP1 inputP1
  print n

  let inputP2 = map parseP2Input $ lines entries
  putStr "Advent of Code Day 8, Part 2: "
  let n' = solveP2 inputP2
  print n'
