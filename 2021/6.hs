{-# OPTIONS_GHC -Wno-incomplete-patterns #-}

import           Data.Array
import           Data.List
import           Data.List.Split

readInt :: String -> Int
readInt = read

main :: IO ()
main = do
  entries <- readFile "2021/input6"
  let input = map readInt $ splitOn "," $ head $ lines entries

  putStr "Advent of Code Day 6, Part 1: "
  let n = solveP1 input
  print n

  putStr "Advent of Code Day 6, Part 2: "
  let n = solveP2 256 input
  print n

timePasses :: Int -> (Int, [Int])
timePasses 0 = (6, [8])
timePasses n = (n - 1, [])

collectFish :: [(Int, [Int])] -> [Int]
collectFish []                         = []
collectFish ((fish, []     ) : fishes) = fish : collectFish fishes
collectFish ((fish, [spawn]) : fishes) = fish : spawn : collectFish fishes

s :: [Int] -> [Int]
s = collectFish . map timePasses

solveP1 :: [Int] -> Int
solveP1 initial = length $ last $ take 81 $ iterate' s initial

-- too slow

solveP2' :: [Int] -> Int
solveP2' initial = length $ last $ take 257 $ iterate' s initial

-- too slow

initialFishery :: Array Int Int
initialFishery = listArray (0, 8) (repeat 0)

listToElemList :: [Int] -> [Int]
listToElemList lst =
  elems $ initialFishery // map toIndexMagnitude prefigureList
 where
  toIndexMagnitude ls = (head ls, length ls)
  prefigureList = group . sort $ lst

shiftDown :: [Int] -> [Int]
shiftDown (i0 : i1 : i2 : i3 : i4 : i5 : i6 : i7 : i8 : _) =
  [i1, i2, i3, i4, i5, i6, i7 + i0, i8, i0]

solveP2 :: Int -> [Int] -> Int
solveP2 n lst =
  let firstFish = listToElemList lst
  in  sum $ last $ take (n + 1) $ iterate shiftDown firstFish
