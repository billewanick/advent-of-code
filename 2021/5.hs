-- https://adventofcode.com/2021/day/5

import           Data.List
import           Data.List.Split

input =
  [ "0,9 -> 5,9"
  , "8,0 -> 0,8"
  , "9,4 -> 3,4"
  , "2,2 -> 2,1"
  , "7,0 -> 7,4"
  , "6,4 -> 2,0"
  , "0,9 -> 2,9"
  , "3,4 -> 1,4"
  , "0,0 -> 8,8"
  , "5,5 -> 8,2"
  ]

readInt :: String -> Int
readInt = read

processInput :: [[Char]] -> [[[Int]]]
processInput = map (map (map readInt . splitOn ",") . splitOn " -> ")

x :: [[[Int]]]
x = processInput input

x' :: [[Int]]
x' = head x

prettyPrint :: [[[Int]]] -> IO ()
prettyPrint = putStrLn . unlines . map show

isHorizontalOrVertical :: [[Int]] -> Bool
isHorizontalOrVertical dataRow = x1 == x2 || y1 == y2
 where
  [x1, x2] = map head dataRow
  [y1, y2] = map last dataRow

getStraightVents :: [[[Int]]] -> [[[Int]]]
getStraightVents = filter isHorizontalOrVertical

getP1AffectedPoints :: [[Int]] -> [(Int, Int)]
getP1AffectedPoints dataRow = [ (x, y) | x <- [x1 .. x2], y <- [y1 .. y2] ]
 where
  [x1, x2] = sort $ map head dataRow
  [y1, y2] = sort $ map last dataRow

solveP1 :: [[[Int]]] -> Int
solveP1 processed =
  length
    $ filter (\x -> length x >= 2)
    $ group
    $ sort
    $ concatMap getP1AffectedPoints
    $ getStraightVents processed

main :: IO ()
main = do
  entries <- readFile "2021/input5"
  let processed = processInput $ lines entries

  putStr "Advent of Code Day 5, Part 1: "
  let n = solveP1 processed
  print n

  putStr "Advent of Code Day 5, Part 2: "
  let n = solveP2 processed
  print n

getP2AffectedPoints :: [[Int]] -> [(Int, Int)]
getP2AffectedPoints dataRow = zip xs ys
 where
  [x1, x2] = map head dataRow
  [y1, y2] = map last dataRow
  xs       = if x1 > x2 then [x1, x1 - 1 .. x2] else [x1 .. x2]
  ys       = if y1 > y2 then [y1, y1 - 1 .. y2] else [y1 .. y2]

solveP2 :: [[[Int]]] -> Int
solveP2 processed =
  let (straightLines, diagonalLines) =
        partition isHorizontalOrVertical processed
      strs      = concatMap getP1AffectedPoints straightLines
      diags     = concatMap getP2AffectedPoints diagonalLines
      allPoints = strs <> diags
  in  length $ filter (\x -> length x >= 2) $ group $ sort allPoints
