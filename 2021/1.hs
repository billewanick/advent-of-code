{-
Advent of Code 2021
https://adventofcode.com/2021/day/1
-}

import           Data.List                      ( )

-- getting input data from file
entries :: IO [Int]
entries = map read . lines <$> readFile "2021/input1" :: IO [Int]

-- solution
f :: [Int] -> [Int]
f (a : b : xs) = (if b > a then 1 else 0) : f (b : xs)
f _            = []

solveP1 :: [Int] -> Int
solveP1 = sum . f

f' :: [Int] -> [Int]
f' (a : b : c : d : xs) =
  (if sum [a, b, c] < sum [b, c, d] then 1 else 0) : f' (b : c : d : xs)
f' _ = []

solveP2 :: [Int] -> Int
solveP2 = sum . f'

input :: [Int]
input = [199, 200, 208, 210, 200, 207, 240, 269, 260, 263]
