{-
Advent of Code 2021
https://adventofcode.com/2021/day/2
-}

{-# OPTIONS_GHC -Wno-incomplete-patterns #-}

import           System.IO.Unsafe               ( unsafePerformIO )
import           Text.Parsec                    ( ParseError
                                                , Parsec
                                                , char
                                                , digit
                                                , letter
                                                , many1
                                                , string
                                                )
import qualified Text.Parsec                   as Parsec
import           Text.Parsec.Expr               ( )

-- | 'Parser' is a convenience type for 'Parsec'
type Parser = Parsec String ()

-- | The 'parse' function is a convenience function for 'Parsec.parse' that
-- removes the requirement to provide a file name.
parse
  ::
  -- | The parser for "a"s
     Parser a
  ->
  -- | The string to be parsed
     String
  ->
  -- | The successfully parsed value or an error
     Either ParseError a
parse p = Parsec.parse p ""

-- | The 'parselist' function parses a list of 'String's using 'parse' and
-- returns the list of parsed "a"s.  If any parse was unsuccessful we crash the
-- program, showing the first error encountered.
parselist
  ::
  -- | The parser for "a"s
     Parser a
  ->
  -- | The list of 'String's to parse
     [String]
  ->
  -- | The resulting list of "a"s
     [a]
parselist p = either (error . show) id . mapM (parse p)

p :: Parser (Direction, Int)
p = do
  command <- many1 letter
  char ' '
  magnitude <- many1 digit
  return (command' command, read magnitude)
 where
  command' c | c == "forward" = Forward
             | c == "up"      = Up
             | c == "down"    = Down
             | otherwise      = error "bad parse"

input :: [String]
input = ["forward 5", "down 5", "forward 8", "up 3", "down 8", "forward 2"]

test :: [(Direction, Int)]
test = parselist p input

-- getting input data from file
entries :: IO [String]
entries = lines <$> readFile "2021/input2"

-- solution
-- imagine the parsing happens successfully into a list of (Direction, Int)s
-- Part 1

type Depth = Int
type Horizontal = Int

data Direction = Forward | Up | Down
  deriving (Show, Eq)

f :: (Direction, Int) -> (Depth, Horizontal)
f (Forward, n) = (0, n)
f (Up     , n) = (-n, 0)
f (Down   , n) = (n, 0)

solveP1 :: [String] -> Int
solveP1 rawInputs = h * d
 where
  inputs  = parselist p rawInputs
  weights = map f inputs
  d       = (sum . map fst) weights
  h       = (sum . map snd) weights

-- Part 2

type Aim = Int

f' :: (Depth, Horizontal, Aim) -> (Direction, Int) -> (Depth, Horizontal, Aim)
f' (d, h, a) (d', n) | d' == Down    = (d, h, a + n)
                     | d' == Up      = (d, h, a - n)
                     | d' == Forward = (d + a * n, h + n, a)

solveP2 :: [String] -> Int
solveP2 rawInputs = h * d
 where
  inputs    = parselist p rawInputs
  (d, h, a) = foldl f' (0, 0, 0) inputs

-- main
main :: IO ()
{-# NOINLINE main #-}
main = do
  putStrLn "Day 2 Advent of Code"
  putStrLn $ "Puzzle 1: " <> show (solveP1 strs)
  putStrLn $ "Puzzle 2: " <> show (solveP2 strs)
  where strs = unsafePerformIO entries
