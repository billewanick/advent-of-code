# Advent of Code in Haskell

Started with a few 2020 exercises, continuing on with 2021 as they come out.

To run:

- first, install [Nix](https://nixos.org/download.html#nix-quick-install) - `curl -L https://nixos.org/nix/install | sh`
- Then, from root of project, run `nix develop`
  - Alternatively you can use [direnv](https://github.com/nix-community/nix-direnv), although if you know what that is you probably don't need my help
- In the bash shell with all the dependencies installed, run `ghci 2021/1.hs`
  - Replace `1.hs` with which ever day you're working on
- From `ghci> `, run whichever function solves the problem
  - For `1.hs`, it would be `solveP1` or `solveP2`

This is meant to be run interactively with GHCi, most files won't contain a main function.
I do it this way because I use the interactivity of the REPL to help solve the problem
