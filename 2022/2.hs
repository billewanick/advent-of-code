{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

import           Relude

import           Data.Text (uncons, unsnoc)

readInteger :: Text -> Maybe Integer
readInteger = readMaybe . toString

data GameOutcome
  = Win
  | Loss
  | Tie
  deriving (Show, Eq)

data Hand
  = Rock
  | Paper
  | Scissors
  deriving (Show, Eq)

main :: IO ()
main = do
  entries <- lines <$> decodeUtf8 <$> readFileBS "input2"
  let guideMovesP1 = map toGuideP1 entries
  let guideMovesP2 = map toGuideP2 entries
  print ("Part 1: " <> show (solveP1 guideMovesP1))
  print ("Part 2: " <> show (solveP2 guideMovesP2))

toGuideP1 :: Text -> (Hand, Hand)
toGuideP1 txt = (stringToHand head', stringToHand last')
  where
    head' = maybe 'A' fst (Data.Text.uncons txt)
    last' = maybe 'X' snd (Data.Text.unsnoc txt)
    {-
      Their moves       Your moves
      A is Rock         X is Rock
      B is Paper        Y is Paper
      C is Scissors     Z is Scissors
    -}
    stringToHand :: Char -> Hand
    stringToHand 'A' = Rock
    stringToHand 'B' = Paper
    stringToHand 'C' = Scissors
    stringToHand 'X' = Rock
    stringToHand 'Y' = Paper
    stringToHand 'Z' = Scissors
    stringToHand _   = Rock

-- outcome for me, the snd player
outcomeP1 :: (Hand, Hand) -> Integer
outcomeP1 (Rock, Rock)         = scoringGame Tie  + scoringHand Rock
outcomeP1 (Rock, Paper)        = scoringGame Win  + scoringHand Paper
outcomeP1 (Rock, Scissors)     = scoringGame Loss + scoringHand Scissors

outcomeP1 (Paper, Rock)        = scoringGame Loss + scoringHand Rock
outcomeP1 (Paper, Paper)       = scoringGame Tie  + scoringHand Paper
outcomeP1 (Paper, Scissors)    = scoringGame Win  + scoringHand Scissors

outcomeP1 (Scissors, Rock)     = scoringGame Win  + scoringHand Rock
outcomeP1 (Scissors, Paper)    = scoringGame Loss + scoringHand Paper
outcomeP1 (Scissors, Scissors) = scoringGame Tie  + scoringHand Scissors

scoringGame :: GameOutcome -> Integer
scoringGame Win  = 6
scoringGame Loss = 0
scoringGame Tie  = 3

scoringHand :: Hand -> Integer
scoringHand Rock     = 1
scoringHand Paper    = 2
scoringHand Scissors = 3

solveP1 :: [(Hand, Hand)] -> Integer
solveP1 = sum . map outcomeP1

solveP2 :: [(Hand, GameOutcome)] -> Integer
solveP2 = sum . map outcomeP2

toGuideP2 :: Text -> (Hand, GameOutcome)
toGuideP2 txt = (stringToHand head', stringToOutcome last')
  where
    head' = maybe 'A' fst (Data.Text.uncons txt)
    last' = maybe 'X' snd (Data.Text.unsnoc txt)
    {-
      Their moves       Your outcome
      A is Rock         X is Loss
      B is Paper        Y is Tie
      C is Scissors     Z is Win
    -}
    stringToHand :: Char -> Hand
    stringToHand 'A' = Rock
    stringToHand 'B' = Paper
    stringToHand 'C' = Scissors
    stringToHand _   = Rock

    stringToOutcome 'X' = Loss
    stringToOutcome 'Y' = Tie
    stringToOutcome 'Z' = Win
    stringToOutcome _   = Loss

-- outcome for me, the snd player
outcomeP2 :: (Hand, GameOutcome) -> Integer
outcomeP2 (Rock, Loss)     = scoringGame Loss + scoringHand Scissors
outcomeP2 (Rock, Tie)      = scoringGame Tie  + scoringHand Rock
outcomeP2 (Rock, Win)      = scoringGame Win  + scoringHand Paper

outcomeP2 (Paper, Loss)    = scoringGame Loss + scoringHand Rock
outcomeP2 (Paper, Tie)     = scoringGame Tie  + scoringHand Paper
outcomeP2 (Paper, Win)     = scoringGame Win  + scoringHand Scissors

outcomeP2 (Scissors, Loss) = scoringGame Loss + scoringHand Paper
outcomeP2 (Scissors, Tie)  = scoringGame Tie  + scoringHand Scissors
outcomeP2 (Scissors, Win)  = scoringGame Win  + scoringHand Rock
