{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

import           Relude

import           Data.List       (intersect)
import           Data.List.Split (chunksOf)
import qualified Data.Text       as T

main :: IO ()
main = do
  entries <- lines <$> decodeUtf8 <$> readFileBS "input3"

  print ("Part 1: " <> show (solveP1 entries))
  print ("Part 2: " <> show (solveP2 entries))

solveP1 :: [Text] -> Integer
solveP1 = sum . map (getPriority . findSharedItem . splitTextInTwo)

solveP2 :: [Text] -> Integer
solveP2 = sum . map func . chunksOf 3
  where
    func [a',b',c'] = getPriority $ T.head $ T.pack $ union a b c
      where
        a = toString a'
        b = toString b'
        c = toString c'
        union a b c = a `intersect` b `intersect` c
    func _ = 0

splitTextInTwo :: Text -> (Text, Text)
splitTextInTwo txt = T.splitAt halfLen txt
  where
    halfLen = T.length txt `div` 2

findSharedItem :: (Text, Text) -> Char
findSharedItem (a,b) = T.head $ T.filter (`elem` b) a
  where
    elem c txt = T.any (== c) txt

getPriority :: Char -> Integer
getPriority c = maybe 0 snd (lookup c priorities)
  where
    lookup :: Char -> [(Char, Integer)] -> Maybe (Char, Integer)
    lookup d = find (\(c,n) -> c == d)

priorities :: [(Char, Integer)]
priorities = zip (['a'..'z'] ++ ['A'..'Z']) [1..]
