{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

import           Relude
import           Text.Parsec
import           Text.Parsec.String
import           Text.Read          (read)

main :: IO ()
main = do
  entries <- fromRight (error "data didn't parse")
          <$> parseFile text "input7"
  print "Advent of Code 2022 - Day 7"
  print (lines entries)
  print ("Part 1: " <> solveP1)
  print ("Part 2: " <> solveP2)

solveP1 = undefined

solveP2 = undefined


-- Example data
ex =
  [ "$ cd /"
  , "$ ls"
  , "dir a"
  , "14848514 b.txt"
  , "8504156 c.dat"
  , "dir d"
  , "$ cd a"
  , "$ ls"
  , "dir e"
  , "29116 f"
  , "2557 g"
  , "62596 h.lst"
  , "$ cd e"
  , "$ ls"
  , "584 i"
  , "$ cd .."
  , "$ cd .."
  , "$ cd d"
  , "$ ls"
  , "4060174 j"
  , "8033020 d.log"
  , "5626152 d.ext"
  , "7214296 k"
  ]

-- Data structures
data Directory files = Directory FilePath [files]
                     | File      FilePath Integer
  deriving (Show)


-- Parsing for question
integer :: Parser Integer
integer = do
  n <- many1 digit
  pure $ read n

text :: Parser Text
text = do
  txt <- many1 anyChar
  pure $ toText txt

command :: Parser Text
command = do
  string "$ "
  cmd <- try $ choice [ls, cd]
  pure cmd

cd :: Parser Text
cd = do
  string "cd "
  dir <- text
  pure dir

ls :: Parser Text
ls = do
  string "ls"
  pure "ls"

file :: Parser (Text, Integer)
file = do
  size <- integer
  string " "
  fileName <- text
  pure (fileName, size)

directoryName :: Parser Text
directoryName = do
  string "dir "
  dirName <- text
  pure dirName

-- questionData :: Parser ([Text], [(Integer, (Integer, Integer))])
-- questionData = do
--   input <- try $ choice [command, file, directoryName]
--   pure input
questionData = undefined


-- Parse an entire file with the given Parser.
parseFile :: Parser a -> FilePath -> IO (Either ParseError a)
parseFile p fp = parse p fp . decodeUtf8 <$> readFileBS fp

-- Parse each line of a file with the given Parser.
parseFileLines :: Parser a -> FilePath -> IO (Either ParseError [a])
parseFileLines p = parseFile p'
  where p' = p `sepBy` endOfLine >>= \ps -> eof >> return ps
