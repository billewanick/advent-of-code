{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Map           as M
import qualified Data.Text          as T
import           Relude
import           Text.Parsec
import           Text.Parsec.String
import           Text.Read          (read)

main :: IO ()
main = do
  (crates', moveList) <- fromRight (error "data didn't parse")
                      <$> parseFile questionData "input5"
  let toMap  = M.fromList . zip [1..]
      crates = toMap $ map (T.replace "_" "") $ T.transpose crates'
      moves  = toMap                                        moveList
  print "Advent of Code 2022 - Day 5"
  print ("Part 1: " <> solveP1 crates moves)
  print ("Part 2: " <> solveP2 crates moves)

solveP1 :: Map Integer Text -> Map Integer (Integer, (Integer, Integer)) -> Text
solveP1 crates moves =
  let finalMap = M.foldl' doAMoveP1 crates moves
  in toText . Relude.map T.head $ M.elems finalMap

solveP2 :: Map Integer Text -> Map Integer (Integer, (Integer, Integer)) -> Text
solveP2 crates moves =
  let finalMap = M.foldl' doAMoveP2 crates moves
  in toText . Relude.map T.head $ M.elems finalMap

doAMoveP1 :: Map Integer Text -> (Integer, (Integer, Integer)) -> Map Integer Text
doAMoveP1 crates (amount, (from, to)) =
  let amount' = fromInteger amount
      fromStack = crates M.! from
      toStack   = crates M.! to
      (movingCrate, oldStack) = T.splitAt amount' fromStack

      -- reverse it to simulate moving one at a time
      newToStack = T.reverse movingCrate <> toStack

      updateToCrate   = M.update (\s -> Just newToStack) to   crates
      updateFromCrate = M.update (\s -> Just oldStack)   from updateToCrate
  in updateFromCrate

doAMoveP2 :: Map Integer Text -> (Integer, (Integer, Integer)) -> Map Integer Text
doAMoveP2 crates (amount, (from, to)) =
  let amount' = fromInteger amount
      fromStack = crates M.! from
      toStack   = crates M.! to
      (movingCrate, oldStack) = T.splitAt amount' fromStack

      -- don't reverse it because the Crate Mover 9001 can lift multiples at a time
      newToStack = movingCrate <> toStack

      updateToCrate   = M.update (\s -> Just newToStack) to   crates
      updateFromCrate = M.update (\s -> Just oldStack)   from updateToCrate
  in updateFromCrate


-- Example data
exampleCrates :: Map Integer Text
exampleCrates = M.fromList $ zip [1..]
  ["CJFCNDBW","NTSLQVZP","RHTJGBZP","PBZJLTD","LMSJBVG","DLFQSP","BFDVB","RMSP","TDV"]
exampleMoves = [(4,(9,6)),(7,(2,5)),(3,(5,2))]
move' = (4, (9,6))
exampleMoves' = M.fromList $ zip [1..] exampleMoves

{-
              [C]         [N] [R]
  [J] [T]     [H]         [P] [L]
  [F] [S] [T] [B]         [M] [D]
  [C] [L] [J] [Z] [S]     [L] [B]
  [N] [Q] [G] [J] [J]     [F] [F] [R]
  [D] [V] [B] [L] [B] [Q] [D] [M] [T]
  [B] [Z] [Z] [T] [V] [S] [V] [S] [D]
  [W] [P] [P] [D] [G] [P] [B] [P] [V]
   1   2   3   4   5   6   7   8   9

  move 4 from 9 to 6
  move 7 from 2 to 5
  move 3 from 5 to 2
  move 2 from 2 to 1
-}


-- Parsing for question
integer :: Parser Integer
integer = do
  n <- many1 digit
  pure $ read n

fullCrate :: Parser Text
fullCrate = do
  string "["
  value <- letter
  string "]"
  pure $ T.singleton value

emptyCrate :: Parser Text
emptyCrate = do
  string "   " -- 3 spaces
  pure "_"

crate :: Parser Text
crate = do
  n <- try $ choice [fullCrate, emptyCrate]
  Text.Parsec.optional $ string " "
  pure n

crateRow :: Parser Text
crateRow = do
  n <- many1 crate
  newline
  pure $ T.concat n

move :: Parser (Integer, (Integer, Integer))
move = do
  string "move "
  num <- integer
  string " from "
  start <- integer
  string " to "
  end <- integer
  newline
  pure (num, (start, end))

questionData :: Parser ([Text], [(Integer, (Integer, Integer))])
questionData = do
  ns <- many1 crateRow
  string " 1   2   3   4   5   6   7   8   9 \n"
  newline
  ms <- many1 move
  eof
  pure (ns, ms)

-- Parse an entire file with the given Parser.
parseFile :: Parser a -> FilePath -> IO (Either ParseError a)
parseFile p fp = parse p fp . decodeUtf8 <$> readFileBS fp

-- Parse each line of a file with the given Parser.
parseFileLines :: Parser a -> FilePath -> IO (Either ParseError [a])
parseFileLines p = parseFile p'
  where p' = p `sepBy` endOfLine >>= \ps -> eof >> return ps
