{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

import qualified Data.HashSet       as HS
import qualified Data.List          as Lst
import qualified Data.Text          as Txt
import           Relude
import           Text.Parsec
import           Text.Parsec.String
import           Text.Read          (read)

main :: IO ()
main = do
  entries <- fromRight (error "data didn't parse")
          <$> parseFile questionData "input6"
  print "Advent of Code 2022 - Day 6"
  print ("Part 1: " <> (show $ solveP1 entries))
  print ("Part 2: " <> (show $ solveP2 entries))

solveP1 :: Text -> Integer
solveP1 buffer =
  let err = error "no results"
      allEnds = Txt.tails buffer
      allMarkers = map (Txt.take 4) allEnds
      correctMarker = viaNonEmpty head $ filter allUnique allMarkers
      justMarker = fromMaybe err correctMarker
  in (+) 4 $ -- add 4 since it's minimum allowed
    toInteger . fromMaybe err $ Lst.elemIndex justMarker allMarkers

solveP2 :: Text -> Integer
solveP2 buffer =
  let err = error "no results"
      allEnds = Txt.tails buffer
      allMessages = map (Txt.take 14) allEnds
      correctMessage = viaNonEmpty head $ filter allUnique allMessages
      justMessage = fromMaybe err correctMessage
  in (+) 14 $ -- add 14 since it's minimum allowed
    toInteger . fromMaybe err $ Lst.elemIndex justMessage allMessages

allUnique :: Text -> Bool
allUnique txt = textLen == hashSize
  where
    textLen = Txt.length txt
    hashSize = HS.size $ HS.fromList (toString txt)


-- Example data
a, b, c, d :: Text
a = "bvwbjplbgvbhsrlpgdmjqwftvncz"      --  first marker after character 5
b = "nppdvjthqldpwncqszvftbrmjlhg"      --  first marker after character 6
c = "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg" --  first marker after character 10
d = "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"  --  first marker after character 11


-- Parsing for question
integer :: Parser Integer
integer = do
  n <- many1 digit
  pure $ read n

questionData :: Parser Text
questionData = do
  input <- many1 letter
  pure $ toText input

-- Parse an entire file with the given Parser.
parseFile :: Parser a -> FilePath -> IO (Either ParseError a)
parseFile p fp = parse p fp . decodeUtf8 <$> readFileBS fp

-- Parse each line of a file with the given Parser.
parseFileLines :: Parser a -> FilePath -> IO (Either ParseError [a])
parseFileLines p = parseFile p'
  where p' = p `sepBy` endOfLine >>= \ps -> eof >> return ps
