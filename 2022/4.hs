{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

import           Relude
import           Text.Parsec
import           Text.Parsec.String
import           Text.Read

main :: IO ()
main = do
  entries <- fromMaybe [] . rightToMaybe <$> parseFileLines assignmentPairs "input4"
  print "Advent of Code 2022 - Day 4"
  print ("Part 1: " <> show (solveP1 entries))
  print ("Part 2: " <> show (solveP2 entries))

solveP1 :: [((Integer, Integer), (Integer, Integer))] -> Integer
solveP1 = genericLength . filter fullyContains
  where
    fullyContains :: ((Integer, Integer), (Integer, Integer)) -> Bool
    fullyContains ((start, end), (start', end'))
      | start  <= start' && end  >= end' = True
      | start' <= start  && end' >= end  = True
      | otherwise                        = False

solveP2 :: [((Integer, Integer), (Integer, Integer))] -> Integer
solveP2 = genericLength . filter overlaps
  where
    overlaps :: ((Integer, Integer), (Integer, Integer)) -> Bool
    overlaps ((start, end), (start', end'))
      | end >= start' && start <= end' = True
      | otherwise                      = False

-- Example data
-- `solveP1 example`
example =
  [ ( (2,4) , (6,8) )
  , ( (2,3) , (4,5) )
  , ( (5,7) , (7,9) )
  , ( (2,8) , (3,7) )
  , ( (6,6) , (4,6) )
  , ( (2,6) , (4,8) )
  ]

-- Parsing for question
integer :: Parser Integer
integer = do
  n <- many1 digit
  return (read n)

section :: Parser (Integer, Integer)
section = do
  start <- integer
  string "-"
  end <- integer
  return (start, end)

assignmentPairs :: Parser ((Integer, Integer), (Integer, Integer))
assignmentPairs = do
  first <- section
  string ","
  second <- section
  return (first, second)

-- Parse an entire file with the given Parser.
parseFile :: Parser a -> FilePath -> IO (Either ParseError a)
parseFile p fp = parse p fp . decodeUtf8 <$> readFileBS fp

-- Parse each line of a file with the given Parser.
parseFileLines :: Parser a -> FilePath -> IO (Either ParseError [a])
parseFileLines p = parseFile p'
  where p' = p `sepBy` endOfLine >>= \ps -> eof >> return ps
