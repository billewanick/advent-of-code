{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

import           Relude
import           Text.Parsec
import           Text.Parsec.String
import           Text.Read          (read)

main :: IO ()
main = do
  entries <- fromRight (error "data didn't parse")
          <$> parseFile questionData "input?"
  print "Advent of Code 2022 - Day ?"
  print ("Part 1: " <> solveP1)
  print ("Part 2: " <> solveP2)

solveP1 = undefined

solveP2 = undefined


-- Example data


-- Parsing for question
integer :: Parser Integer
integer = do
  n <- many1 digit
  pure $ read n

-- questionData :: Parser ([Text], [(Integer, (Integer, Integer))])
questionData = do
  undefined

-- Parse an entire file with the given Parser.
parseFile :: Parser a -> FilePath -> IO (Either ParseError a)
parseFile p fp = parse p fp . decodeUtf8 <$> readFileBS fp

-- Parse each line of a file with the given Parser.
parseFileLines :: Parser a -> FilePath -> IO (Either ParseError [a])
parseFileLines p = parseFile p'
  where p' = p `sepBy` endOfLine >>= \ps -> eof >> return ps
