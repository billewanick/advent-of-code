{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

import qualified Data.HashSet       as HS
import qualified Data.List          as Lst
import qualified Data.Text          as Txt
import           Relude
import           Text.Parsec
import           Text.Parsec.String
import           Text.Read          (read)

main :: IO ()
main = do
  entries <- fromRight (error "data didn't parse")
          <$> parseFile questionData "input8"
  print "Advent of Code 2022 - Day 8"
  print ("Part 1: " <> (show $ solveP1 ex1))
  -- print ("Part 2: " <> (show $ solveP2 entries))

solveP1 :: [[Integer]] -> Integer
solveP1 = sum . map sum

isVisible :: [[Integer]] -> (Integer, Integer) -> Bool
isVisible forest (x, y) = trace neighbours $ all isVisible' neighbours
  where
    neighbours =
      [ (x+1, y) -- Right
      , (x-1, y) -- Left
      , (x, y+1) -- Up
      , (x, y-1) -- Down
      ]

    isVisible' :: (Integer, Integer) -> Bool
    isVisible' = undefined

generateCoords = [ (x,y) | x <- [1..5], y <- [1..5] ]

-- solveP2 :: Text -> Integer
-- solveP2 = undefined


-- Example data
ex =
  [ 30373
  , 25512
  , 65332
  , 33549
  , 35390
  ]

ex1 :: [ [ Integer ] ]
ex1 =
  [ [ 3, 0, 3, 7, 3 ]
  , [ 2, 5, 5, 1, 2 ]
  , [ 6, 5, 3, 3, 2 ]
  , [ 3, 3, 5, 4, 9 ]
  , [ 3, 5, 3, 9, 0 ]
  ]
  -- print ("Part 1: " <> (show $ solveP1 entries))
  -- print ("Part 2: " <> (show $ solveP2 entries))

-- solveP1 :: Text -> Integer
solveP1 = undefined

-- solveP2 :: Text -> Integer
solveP2 = undefined


-- Example data

  print ("Part 1: " <> (show $ solveP1 ex1))
  -- print ("Part 2: " <> (show $ solveP2 entries))

solveP1 :: [[Integer]] -> Integer
solveP1 = sum . map sum

isVisible :: [[Integer]] -> (Integer, Integer) -> Bool
isVisible forest (x, y) = trace neighbours $ all isVisible' neighbours
  where
    neighbours =
      [ (x+1, y) -- Right
      , (x-1, y) -- Left
      , (x, y+1) -- Up
      , (x, y-1) -- Down
      ]

    isVisible' :: (Integer, Integer) -> Bool
    isVisible' = undefined

generateCoords = [ (x,y) | x <- [1..5], y <- [1..5] ]

-- solveP2 :: Text -> Integer
-- solveP2 = undefined


-- Example data
ex =
  [ 30373
  , 25512
  , 65332
  , 33549
  , 35390
  ]

ex1 :: [ [ Integer ] ]
ex1 =
  [ [ 3, 0, 3, 7, 3 ]
  , [ 2, 5, 5, 1, 2 ]
  , [ 6, 5, 3, 3, 2 ]
  , [ 3, 3, 5, 4, 9 ]
  , [ 3, 5, 3, 9, 0 ]
  ]
  -- print ("Part 1: " <> (show $ solveP1 entries))
  -- print ("Part 2: " <> (show $ solveP2 entries))

-- solveP1 :: Text -> Integer
solveP1 = undefined

-- solveP2 :: Text -> Integer
solveP2 = undefined


-- Example data


-- Parsing for question
integer :: Parser Integer
integer = do
  n <- many1 digit
  pure $ read n

questionData :: Parser [Integer]
questionData = do
  input <- many1 integer
  pure $ input
questionData :: Parser Text
questionData = do
  input <- many1 letter
  pure $ toText input
questionData :: Parser [Integer]
questionData = do
  input <- many1 integer
  pure $ input
questionData :: Parser Text
questionData = do
  input <- many1 letter
  pure $ toText input

-- Parse an entire file with the given Parser.
parseFile :: Parser a -> FilePath -> IO (Either ParseError a)
parseFile p fp = parse p fp . decodeUtf8 <$> readFileBS fp

-- Parse each line of a file with the given Parser.
parseFileLines :: Parser a -> FilePath -> IO (Either ParseError [a])
parseFileLines p = parseFile p'
  where p' = p `sepBy` endOfLine >>= \ps -> eof >> return ps
