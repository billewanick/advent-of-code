{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

import           Relude

import           Data.List (maximum, sortOn)
import           Data.Ord  (Down (Down))
import           Data.Text (splitOn)

readInteger :: Text -> Maybe Integer
readInteger = readMaybe . toString

solveP1 :: [[Integer]] -> Integer
solveP1 = maximum . map sum

solveP2 :: [[Integer]] -> Integer
solveP2 = sum . take 3 . sortOn Down . map sum

main :: IO ()
main = do
  entries <- map (mapMaybe readInteger . lines) . splitOn "\n\n" <$> decodeUtf8 <$> readFileBS "input1"

  -- print entries
  print ("Part 1: " <> show (solveP1 entries))
  print ("Part 2: " <> show (solveP2 entries))
