{-
Trying 2020 Advent of Code as warmup
https://adventofcode.com/2020/day/2
-}

import qualified Data.ByteString.Char8 as B
import Data.List (group, sort)
import Debug.Trace (trace)
import System.IO.Unsafe
import Text.Parsec
  ( ParseError,
    Parsec,
    char,
    digit,
    letter,
    many1,
    string,
  )
import qualified Text.Parsec as Parsec
import Text.Parsec.Expr ()

-- | 'Parser' is a convenience type for 'Parsec'
type Parser = Parsec String ()

-- | The 'parse' function is a convenience function for 'Parsec.parse' that
-- removes the requirement to provide a file name.
parse ::
  -- | The parser for "a"s
  Parser a ->
  -- | The string to be parsed
  String ->
  -- | The successfully parsed value or an error
  Either ParseError a
parse p = Parsec.parse p ""

-- | The 'parselist' function parses a list of 'String's using 'parse' and
-- returns the list of parsed "a"s.  If any parse was unsuccessful we crash the
-- program, showing the first error encountered.
parselist ::
  -- | The parser for "a"s
  Parser a ->
  -- | The list of 'String's to parse
  [String] ->
  -- | The resulting list of "a"s
  [a]
parselist p = either (error . show) id . mapM (parse p)

p :: Parser (Int, Int, Char, String)
p = do
  low <- many1 digit
  char '-'
  high <- many1 digit
  char ' '
  c <- letter
  string ": "
  s <- many1 letter
  return (read low, read high, c, s)

isValid :: (Int, Int, Char, String) -> Bool
isValid (low, high, c, s) = lenCs >= low && lenCs <= high
  where
    lenCs = if not (null result) then length $ head result else 0
    result = filter (\str -> head str == c) cs
    cs = (group . sort) s

solveP1 = length $ filter (== True) $ map isValid (unsafePerformIO entries)

-- getting input data from file
entries :: IO [(Int, Int, Char, String)]
entries = parselist p . lines <$> readFile "input2" :: IO [(Int, Int, Char, String)]

input =
  [ "1-3 a: abcde",
    "1-3 b: cdefg",
    "2-9 c: ccccccccc"
  ]
