{-
Trying 2020 Advent of Code as warmup
First Question
https://adventofcode.com/2020/day/1
-}

import Data.List ()

-- getting input data from file
entries :: IO [Integer]
entries = map read . lines <$> readFile "input1" :: IO [Integer]

-- solution
solve1a :: [Integer] -> Integer
solve1a lst = product $ filter (`elem` lst) lst'
  where
    lst' = complement lst

solve1b lst = head $ [i * j * k | i <- lst, j <- lst, k <- lst, i + j + k == 2020]

complement :: [Integer] -> [Integer]
complement = map (abs . (2020 -))

-- Test input for Repl
input' :: [Integer]
input' = complement input

input :: [Integer]
input =
  [ 1721,
    979,
    366,
    299,
    675,
    1456,
    3000
  ]